var gulp = require('gulp'),
    source = 'node_modules',
    desc = 'public',
    gulpMerge = require('gulp-merge');

gulp.task('plugins',function() {
    return gulpMerge(
        gulp.src([source + '/jquery/dist/css/*.min.css']).pipe(gulp.dest(desc +'/css/jquery/css')),
        gulp.src([source + '/jquery/dist/js/*.min.js']).pipe(gulp.dest(desc +'/js/jquery/js')),
        gulp.src([source + '/bootstrap/dist/css/*.min.css']).pipe(gulp.dest(desc +'/css/bootstrap/css')),
        gulp.src([source + '/bootstrap/dist/js/*.min.js']).pipe(gulp.dest(desc +'/js/bootstrap/js'))
      )
});
gulp.task('default', gulp.series('plugins', function() {
  cb();
}));
