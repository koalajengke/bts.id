var express = require('express');
var app = express();
var routes = require('./routes/routes');
const bodyParser = require('body-parser');

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static('public'));
app.use('/bootstrap',express.static('node_modules/bootstrap/dist'));
app.use('/jquery',express.static('node_modules/jquery/dist'));
app.use('/api', routes.router);

app.get('/', function(req, res){
  var data = {title:'social media',project: ['facebook','twiter','instagram']};
  res.render('index', data);
});



module.exports = app;
