module.exports = function(sequelize, DataTypes) {
  return sequelize.define('books', {
    title: DataTypes.STRING,
    body: DataTypes.STRING,
    author: DataTypes.STRING
  });
};
