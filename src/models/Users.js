module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    email: DataTypes.STRING,
    phone: DataTypes.STRING,
    country: DataTypes.STRING,
    city: DataTypes.STRING,
    postcode: DataTypes.STRING,
    name: DataTypes.STRING,
    address: DataTypes.STRING
  });
};
