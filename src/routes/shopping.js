var express = require('express');
var router = express.Router();
require('dotenv').config();
var con = require('../config/database');
var moment = require('moment');

const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

router.post('/', function(req, res){
  return con.Shopping.create({
    name : req.body.name,
    createddate: moment().format('YYYY-mm-dd H:i:s')
  }).then(function (book) {
      if (book) {
          res.redirect(prefix + '/books')
      } else {
          res.status(400).send('Error in insert new record');
      }
  });

});

router.post('/all', function(req, res){
  return con.Shopping.findall()
  .then(function (shop) {
      if (shop) {
          res.send(shop)
      } else {
          res.status(400).send('Error in insert new record');
      }
  });

});

module.exports.router = router;
