var express = require('express');
var router = express.Router();
require('dotenv').config();
var con = require('../config/database');


const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');

router.post('/signup', function(req, res){
  let hash = bcrypt.hashSync(req.body.password, 10);
  return con.Users.create({
    username : req.body.username,
    email :req.body.email,
    password : hash,
    phone : req.body.phone,
    address : req.body.address,
    city : req.body.city,
    country : req.body.country,
    name : req.body.name,
    postcode : req.body.postcode
  }).then(function(user){
    if(user){
      res.send({users: {
        username : req.body.username,
        email :req.body.email,
        encrypted_password : req.body.password,
        phone : req.body.phone,
        address : req.body.address,
        city : req.body.city,
        country : req.body.country,
        name : req.body.name,
        postcode : req.body.postcode
      }})
    }else{
      res.status(400).send('Error in insert new record');
    }
  });
});

router.post('/signin', function(req, res){

    let email = req.body.email;
    let password = req.body.password;

    return con.Users.findOne({ where:{ email: req.body.email }})
    .then(function(user){
      // if (!user) return res.status(404).send('No user found.');

      var passwordIsValid = bcrypt.compareSync(password, user.password);
      if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });

      var token = jwt.sign({ id: user._id }, process.env.JWT_ENCRYPTION, {
        expiresIn: process.env.JWT_EXPIRATION
      });

      res.status(200).send({email:user.email, token: token, username: user.username });
    }).catch(function(err){
        if (err) return res.status(404).send('wrong email or password');
    })

});


router.get('/', function(req, res){
      return con.Users.findAll()
      .then(function(item){
        var data = {users: item};
        res.send(data);
      })
      .catch(function(err){
        response.send(err);
      });

});

module.exports.router = router;
