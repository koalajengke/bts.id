var express = require('express');
var router = express.Router();

var con = require('../config/database');
const prefix = '/web/v1';

router.get('/', function(req, res){
  con.Books.findAll()
  .then(function(item){
    var data = {books: item};
    res.render('books-list', data);
  })
  .catch(function(err){
    response.send(err);
  });
});

router.post('/create', function(req, res){
  return con.Books.create({
      author: req.body.author,
      title: req.body.title,
      body: req.body.body
  }).then(function (book) {
      if (book) {
          res.redirect(prefix + '/books')
      } else {
          res.status(400).send('Error in insert new record');
      }
  });
});

router.post('/update', function(req, res){
  return con.Books.update({
      author: req.body.author,
      title: req.body.title,
      body: req.body.body
  },{where:{ id: req.body.id } }).then(function (book) {
      if (book) {
          res.redirect(prefix + '/books')
      } else {
          res.status(400).send('Error in updating data');
      }
  });
});

router.post('/delete', function(req, res){
  return con.Books.destroy({where:{ id: req.body.id } }).then(function (book) {
      if (book) {
          res.redirect(prefix + '/books')
      } else {
          res.status(400).send('Error in deleting data');
      }
  });
});

module.exports.router = router;
