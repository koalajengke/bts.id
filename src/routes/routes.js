var express = require('express');
var router = express.Router();

router.use('/books', require('./books').router);
router.use('/users', require('./signup').router);
router.use('/shopping', require('./shopping').router);

router.use(function(req, res, next) {
    if (!req.route)
        return next (new Error('Page not found'));
    next();
});

router.use(function(err, req, res, next){
    res.send(err.message);
});

module.exports.router = router;
