var Sequelize = require('sequelize');
require('dotenv').config();
// Option 1: Passing parameters separately
var sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USENAME, process.env.DB_PASSWORD, {
  host: process.env.DB_HOST,
  dialect: process.env.DB_CONNECTION,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

// load models
var models = ['Books','Users','Shoppings'];
models.forEach(function(model) {
    console.log(__dirname + '/../models/' + model);
    console.log(sequelize.import(__dirname + '/../models/' + model));
    module.exports[model] = sequelize.import(__dirname + '/../models/' + model);
});

// describe relationships
(function(m) {
})(module.exports);

// export connection
module.exports.sequelize = sequelize;
